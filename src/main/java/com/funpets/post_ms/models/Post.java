package com.funpets.post_ms.models;

import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

public class Post {

    private LocalDateTime dateCreation;
    private String account;
    private String text;
    private String image_ref;
    private String video_ref;
    private String git_ref;


    public Post(String account, String text, String image_ref, String video_ref, String git_ref){
        this.account= account;
        this.text= text;
        this.image_ref= image_ref;
        this.video_ref= video_ref;
        this.git_ref= git_ref;
        this.dateCreation= LocalDateTime.now();
    }


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage_ref() {
        return image_ref;
    }

    public void setImage_ref(String image_ref) {
        this.image_ref = image_ref;
    }

    public String getVideo_ref() {
        return video_ref;
    }

    public void setVideo_ref(String video_ref) {
        this.video_ref = video_ref;
    }

    public String getGit_ref() {
        return git_ref;
    }

    public void setGit_ref(String git_ref) {
        this.git_ref = git_ref;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }
}
