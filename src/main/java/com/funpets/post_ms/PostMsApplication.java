package com.funpets.post_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostMsApplication.class, args);
	}

}
