package com.funpets.post_ms.repositories;

import com.funpets.post_ms.models.Post;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PostRepository extends MongoRepository<Post, String> {
    List<Post> findByAccount (String account);
}


