package com.funpets.post_ms.controllers;

import com.funpets.post_ms.models.Post;
import com.funpets.post_ms.repositories.PostRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PostController {

    private final PostRepository postRepository;

    public PostController(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @GetMapping("/post/{account}")
    List<Post> getPostbyAccount(@PathVariable String account) {
        List<Post> post = postRepository.findByAccount(account);
        return post;
    }

    @GetMapping("/posts")
    List<Post> getPosts() {
        List<Post> post = postRepository.findAll();
        return post;
    }

    @PostMapping("/post")
    Post newPost(@RequestBody Post post){
        return postRepository.save(post);
    }


}
